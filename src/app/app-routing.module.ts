import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

/********  App components *********/
/*----- Catalog ------*/
import { CategoriesComponent } from './containers/catalog/categories/categories.component';
import { ProductsComponent } from './containers/catalog/products/products.component';

/*----- Common ------*/
import { PageNotFoundComponent } from './containers/page-not-found/page-not-found.component';
import { HomeComponent } from './containers/home/home.component';
import { ForbiddenPageComponent } from './containers/forbidden-page/forbidden-page.component';

/*----- Users ------*/
import { UsersComponent } from './containers/users/users.component';
import { LoginComponent } from './containers/login/login.component';

/*----- Profile ------*/
import { BusinessAuthoritiesComponent } from './containers/profile/business-authorities/business-authorities.component';
import { BusinessResourcesComponent } from './containers/profile/business-resources/business-resources.component';
import { BusinessActionsComponent } from './containers/profile/business-actions/business-actions.component';

/************* Resolvers **************/
/*----- Catalog ------*/
import { CategoriesResolver } from './services/catalog/categories/categories.resolver';
import { ProductsResolver } from './services/catalog/products/products.resolver';

/*----- Users ------*/
import { UsersResolver } from './services/users/users.resolver';

/*----- Profile ------*/
import { BusinessAuthoritiesResolver } from './services/profiles/business-authorities/business-authorities.resolver';
import { BusinessResourcesResolver } from './services/profiles/business-resources/business-resources.resolver';
import { BusinessActionsResolver } from './services/profiles/business-actions/business-actions.resolver';

/************* Guards **************/
import { AuthGuard } from './guards/auth-guard';
import { AdminGuard } from './guards/admin-guard';


/********* Rootes ****************/
const routes: Routes = [
  // Home component root
  { path: '', component: HomeComponent, canActivate: [AuthGuard] },

  // Login component root
  { path: 'login', component: LoginComponent },

  // Categories component root
  { path : 'categories', component: CategoriesComponent, canActivate: [AuthGuard], resolve : {data: CategoriesResolver} },

  // Products component root
  { path : 'products', component: ProductsComponent, canActivate: [AuthGuard], resolve : {data: ProductsResolver} },

  // users component root
  { path : 'users', component: UsersComponent, canActivate: [AuthGuard, AdminGuard], resolve : {data: UsersResolver} },

  // Authorities component root
  // tslint:disable-next-line: max-line-length
  { path : 'authorities', component: BusinessAuthoritiesComponent, canActivate: [AuthGuard, AdminGuard], resolve : {data: BusinessAuthoritiesResolver} },

  // Resources component root
  // tslint:disable-next-line: max-line-length
  { path : 'resources', component: BusinessResourcesComponent, canActivate: [AuthGuard, AdminGuard], resolve : {data: BusinessResourcesResolver} },

  // Actions component root
  // tslint:disable-next-line: max-line-length
  { path : 'actions', component: BusinessActionsComponent, canActivate: [AuthGuard, AdminGuard], resolve : {data: BusinessActionsResolver} },

  // Forbidden page
  { path: 'forbidden', component: ForbiddenPageComponent, canActivate: [AuthGuard]},

  // Page not found
  { path: '**', component: PageNotFoundComponent, canActivate: [AuthGuard]}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
