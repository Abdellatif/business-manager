export const handleError = (error: any) => {
    if (error.error instanceof ErrorEvent) {
        // A client-side or network error occurred. Handle it accordingly.
        console.error('An error occurred:', error.error.message);
        return;
    }

    console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error.error}`);

    // Launch toast with error message;                }
    alert(`Error : ${error.status}` + `---  Body :  + ${error.error.error}`);
};

export const handleSuccess = (message: string) => {
    // Launch toaste with success message
    alert(message);
};
