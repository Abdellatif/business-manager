import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { select } from '@angular-redux/store';

/*************** Actions and queries *******/
import * as businessResourcesQueries from '../../../../store/profile/business-resources/business-resources.queries';

/********** Models  ************/
import { BusinessResource } from '../../../../models/profile/business-resource';

@Component({
  selector: 'app-business-resource-detail',
  templateUrl: './business-resource-detail.component.html',
  styleUrls: ['./business-resource-detail.component.scss']
})
export class BusinessResourceDetailComponent implements OnInit {
  @select(businessResourcesQueries.businessResource) storeBusinessResource: Observable<BusinessResource>;
  businessResource: BusinessResource;

  constructor() {}

  ngOnInit() {
    this.storeBusinessResource.subscribe((value) => {
      this.businessResource = value;
    });
  }
}
