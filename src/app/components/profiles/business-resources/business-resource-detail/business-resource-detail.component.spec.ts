import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BusinessResourceDetailComponent } from './business-resource-detail.component';

describe('BusinessResourceDetailComponent', () => {
  let component: BusinessResourceDetailComponent;
  let fixture: ComponentFixture<BusinessResourceDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BusinessResourceDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BusinessResourceDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
