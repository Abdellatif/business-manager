import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BusinessResourceCreateComponent } from './business-resource-create.component';

describe('BusinessResourceCreateComponent', () => {
  let component: BusinessResourceCreateComponent;
  let fixture: ComponentFixture<BusinessResourceCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BusinessResourceCreateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BusinessResourceCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
