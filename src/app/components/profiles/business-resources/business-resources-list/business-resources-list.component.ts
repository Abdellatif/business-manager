import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { select } from '@angular-redux/store';

/*************** Models *******/
import { BusinessResource } from '../../../../models/profile/business-resource';

/*************** Actions and queries *******/
import { BusinessResourcesActions } from '../../../../store/profile/business-resources/business-resources.actions';
import * as businessResourcesQueries from '../../../../store/profile/business-resources/business-resources.queries';

@Component({
  selector: 'app-business-resources-list',
  templateUrl: './business-resources-list.component.html',
  styleUrls: ['./business-resources-list.component.scss']
})
export class BusinessResourcesListComponent implements OnInit {
  @select(businessResourcesQueries.getBusinessResources) businessResources: Observable<BusinessResource[]>;

  constructor( private businessResourcesActions: BusinessResourcesActions ) { }

  ngOnInit() {
    this.businessResourcesActions.getBusinessResources();
  }

  // redirect to resource detail
  redirectToDetail(businessResource: BusinessResource) {
    this.businessResourcesActions.selectBusinessResource(businessResource);
  }

  // delete a resource
  remove(id: number) {
    this.businessResourcesActions.removeBusinessResource(id);
  }
}
