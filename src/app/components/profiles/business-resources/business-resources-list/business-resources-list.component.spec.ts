import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BusinessResourcesListComponent } from './business-resources-list.component';

describe('BusinessResourcesListComponent', () => {
  let component: BusinessResourcesListComponent;
  let fixture: ComponentFixture<BusinessResourcesListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BusinessResourcesListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BusinessResourcesListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
