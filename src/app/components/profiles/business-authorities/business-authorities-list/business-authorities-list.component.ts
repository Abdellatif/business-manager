import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { select } from '@angular-redux/store';

/*************** Models *******/
import { BusinessAuthority } from '../../../../models/profile/business-authority';

/*************** Actions and queries *******/
import { BusinessAuthoritiesActions } from '../../../../store/profile/business-authorities/business-authorities.actions';
import * as businessAuthoritiesQueries from '../../../../store/profile/business-authorities/business-authorities.queries';

@Component({
  selector: 'app-business-authorities-list',
  templateUrl: './business-authorities-list.component.html',
  styleUrls: ['./business-authorities-list.component.scss']
})
export class BusinessAuthoritiesListComponent implements OnInit {
  @select(businessAuthoritiesQueries.getBusinessAuthorities) businessAuthorities: Observable<BusinessAuthority[]>;

  constructor( private businessAuthoritiesActions: BusinessAuthoritiesActions ) { }

  ngOnInit() {
    this.businessAuthoritiesActions.getBusinessAuthorities();
  }

  // redirect to action detail
  redirectToDetail(businessAuthority: BusinessAuthority) {
    this.businessAuthoritiesActions.selectBusinessAuthority(businessAuthority);
  }

  // delete an action
  remove(id: number) {
    this.businessAuthoritiesActions.removeBusinessAuthority(id);
  }
}
