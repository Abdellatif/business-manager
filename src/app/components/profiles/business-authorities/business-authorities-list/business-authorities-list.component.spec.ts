import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BusinessAuthoritiesListComponent } from './business-authorities-list.component';

describe('BusinessAuthoritiesListComponent', () => {
  let component: BusinessAuthoritiesListComponent;
  let fixture: ComponentFixture<BusinessAuthoritiesListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BusinessAuthoritiesListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BusinessAuthoritiesListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
