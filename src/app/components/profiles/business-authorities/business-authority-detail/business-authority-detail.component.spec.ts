import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BusinessAuthorityDetailComponent } from './business-authority-detail.component';

describe('BusinessAuthorityDetailComponent', () => {
  let component: BusinessAuthorityDetailComponent;
  let fixture: ComponentFixture<BusinessAuthorityDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BusinessAuthorityDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BusinessAuthorityDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
