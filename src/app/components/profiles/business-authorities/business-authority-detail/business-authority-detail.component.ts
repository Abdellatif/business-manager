import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { select } from '@angular-redux/store';

/*************** Actions and queries *******/
import * as businessAuthoritiesQueries from '../../../../store/profile/business-authorities/business-authorities.queries';

/********** Models  ************/
import { BusinessAuthority } from '../../../../models/profile/business-authority';


@Component({
  selector: 'app-business-authority-detail',
  templateUrl: './business-authority-detail.component.html',
  styleUrls: ['./business-authority-detail.component.scss']
})
export class BusinessAuthorityDetailComponent implements OnInit {
  @select(businessAuthoritiesQueries.businessAuthority) storeBusinessAuthority: Observable<BusinessAuthority>;
  businessAuthority: BusinessAuthority;

  constructor() {}

  ngOnInit() {
    this.storeBusinessAuthority.subscribe((value) => {
      this.businessAuthority = value;
    });
  }
}
