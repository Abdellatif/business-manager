import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';

/*************** Models *******/
import { BusinessAuthority } from 'src/app/models/profile/business-authority';
import { BusinessAction } from 'src/app/models/profile/business-action';

/*************** Actions and queries *******/
import { BusinessAuthoritiesActions } from '../../../../store/profile/business-authorities/business-authorities.actions';

/**************Services *********/
import { BusinessActionsService } from '../../../../services/profiles/business-actions/business-actions.service';

/******** Message handlers **********/
import { handleError } from '../../../../utils/message-handler';

/********* Utils ********/
import { BusinessAuthorityForm } from '../../../../business-forms/profile/business-authority-form';

@Component({
  selector: 'app-business-authority-create',
  templateUrl: './business-authority-create.component.html',
  styleUrls: ['./business-authority-create.component.scss']
})
export class BusinessAuthorityCreateComponent implements OnInit {
  businessAuthority: BusinessAuthority;
  businessActions: BusinessAction[];
  form: FormGroup;

  constructor(private businessAuthoritiesActions: BusinessAuthoritiesActions,
              private businessActionsService: BusinessActionsService,
              private modelForm: BusinessAuthorityForm) {
       this.businessAuthority = new BusinessAuthority();
  }

  ngOnInit() {
    this.loadListes();
    this.form = this.modelForm.createModelForm(this.businessAuthority);
  }

  onSubmit() {
    if ( this.form.invalid ) {
      return;
    }

    // Load authority from form
    this.modelForm.formToModel(this.form, this.businessAuthority);
    this.businessAuthoritiesActions.saveBusinessAAuthority(this.businessAuthority);
  }

  onReset() {
    this.form.reset();
  }

  loadListes() {
    this.businessActionsService.getBusinessActions().subscribe(
      (data: BusinessAction[]) => { this.businessActions = data; },
      (error) => { handleError(error); }
    );
  }

  onCBChange(e) {
    this.modelForm.onCheckboxChange(this.form, e);
  }
}
