import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BusinessAuthorityCreateComponent } from './business-authority-create.component';

describe('BusinessAuthorityCreateComponent', () => {
  let component: BusinessAuthorityCreateComponent;
  let fixture: ComponentFixture<BusinessAuthorityCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BusinessAuthorityCreateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BusinessAuthorityCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
