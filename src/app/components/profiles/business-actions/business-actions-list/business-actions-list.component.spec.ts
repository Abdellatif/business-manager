import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BusinessActionsListComponent } from './business-actions-list.component';

describe('BusinessActionsListComponent', () => {
  let component: BusinessActionsListComponent;
  let fixture: ComponentFixture<BusinessActionsListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BusinessActionsListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BusinessActionsListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
