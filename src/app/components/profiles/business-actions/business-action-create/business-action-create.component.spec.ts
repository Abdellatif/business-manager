import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BusinessActionCreateComponent } from './business-action-create.component';

describe('BusinessActionCreateComponent', () => {
  let component: BusinessActionCreateComponent;
  let fixture: ComponentFixture<BusinessActionCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BusinessActionCreateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BusinessActionCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
