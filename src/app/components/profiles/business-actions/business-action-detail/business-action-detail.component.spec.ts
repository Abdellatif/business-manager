import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BusinessActionDetailComponent } from './business-action-detail.component';

describe('BusinessActionDetailComponent', () => {
  let component: BusinessActionDetailComponent;
  let fixture: ComponentFixture<BusinessActionDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BusinessActionDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BusinessActionDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
