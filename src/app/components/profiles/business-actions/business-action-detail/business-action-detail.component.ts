import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { select } from '@angular-redux/store';

/*************** Actions and queries *******/
import * as businessActionsQueries from '../../../../store/profile/business-actions/business-actions.queries';

/********** Models  ************/
import { BusinessAction } from '../../../../models/profile/business-action';

@Component({
  selector: 'app-business-action-detail',
  templateUrl: './business-action-detail.component.html',
  styleUrls: ['./business-action-detail.component.scss']
})
export class BusinessActionDetailComponent implements OnInit {
  @select(businessActionsQueries.businessAction) storeAction: Observable<BusinessAction>;
  businessAction: BusinessAction;

  constructor() {}

  ngOnInit() {
    this.storeAction.subscribe((value) => {
      this.businessAction = value;
    });
  }
}
