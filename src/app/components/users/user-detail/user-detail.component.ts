import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { select } from '@angular-redux/store';

/*************** Actions and queries *******/
import * as usersQueries from '../../../store/users/users.queries';

/********** Models  ************/
import { User } from '../../../models/users/user';

@Component({
  selector: 'app-user-detail',
  templateUrl: './user-detail.component.html',
  styleUrls: ['./user-detail.component.scss']
})
export class UserDetailComponent implements OnInit {

  @select(usersQueries.user) storeUser: Observable<User>;
  user: User;

  constructor() {}

  ngOnInit() {
    this.storeUser.subscribe((value) => {
      this.user = value;
    });
  }

}
