import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { select } from '@angular-redux/store';

/*************** Actions and queries *******/
import * as productsQueries from '../../../../store/catalog/products/products.queries';

/********** Models  ************/
import { Product } from '../../../../models/catalog/product';

@Component({
  selector: 'app-product-detail',
  templateUrl: './product-detail.component.html',
  styleUrls: ['./product-detail.component.scss']
})
export class ProductDetailComponent implements OnInit {

  @select(productsQueries.product) storeProduct: Observable<Product>;
  product: Product;

  constructor() {}

  ngOnInit() {
    this.storeProduct.subscribe((value) => {
      this.product = value;
    });
  }
}
