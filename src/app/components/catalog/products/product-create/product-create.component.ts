import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';

/*************** Models *******/
import { Product } from '../../../../models/catalog/product';
import {Category} from '../../../../models/catalog/category';

/*************** Actions and queries *******/
import { ProductsActions } from '../../../../store/catalog/products/products.actions';

/**************Services *********/
import {CategoriesService} from '../../../../services/catalog/categories/categories.service';

/******** Message handlers **********/
import { handleError } from '../../../../utils/message-handler';

/********* Utils ********/
import { ProductForm } from '../../../../business-forms/catalog/product-form';

@Component({
  selector: 'app-product-create',
  templateUrl: './product-create.component.html',
  styleUrls: ['./product-create.component.scss']
})
export class ProductCreateComponent implements OnInit {
  product: Product;
  categories: Category[];
  form: FormGroup;

  constructor(
    private productsActions: ProductsActions,
    private categoriesService: CategoriesService,
    private modelForm: ProductForm) {
      this.product = new Product();
  }

  ngOnInit() {
    this.loadListes();
    this.form = this.modelForm.createModelForm(this.product);
  }

  onSubmit() {
    if ( this.form.invalid ) {
      return;
    }

    // Load product from form
    this.modelForm.formToModel(this.form, this.product);
    this.productsActions.saveProduct(this.product);
  }

  onReset() {
    this.form.reset();
  }

  loadListes() {
    this.categoriesService.getCategories().subscribe(
      (data: Category[]) => { this.categories = data; },
      (error) => { handleError(error); }
    );
  }
}
