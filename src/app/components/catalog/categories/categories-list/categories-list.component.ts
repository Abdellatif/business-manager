import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { select } from '@angular-redux/store';

/*************** Models *******/
import { Category } from '../../../../models/catalog/category';

/*************** Actions and queries *******/
import { CategoriesActions } from '../../../../store/catalog/categories/categories.actions';
import * as categoriesQueries from '../../../../store/catalog/categories/categories.queries';

@Component({
  selector: 'app-categories-list',
  templateUrl: './categories-list.component.html',
  styleUrls: ['./categories-list.component.scss']
})
export class CategoriesListComponent implements OnInit {
  @select(categoriesQueries.getCategories) categories: Observable<Category[]>;

  constructor(private categoriesActions: CategoriesActions ) { }

  ngOnInit() {
    this.categoriesActions.getCategories();
  }

  // redirect to category detail
  redirectToCategoryDetail(category: Category) {
    this.categoriesActions.selectCategory(category);
  }

  // delete a category
  remove(id: number) {
    this.categoriesActions.removeCategory(id);
  }
}
