import { Injectable } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { Product } from '../../models/catalog/product';
import { BusinessForm } from '../business-form';

@Injectable({
  providedIn: 'root'
})
export class ProductForm implements BusinessForm {
  createModelForm = (model: Product): FormGroup => {
    const formBuilder = new FormBuilder();
    return formBuilder.group({
        name: new FormControl(
          model.name, {
            validators: [Validators.required],
            updateOn: 'blur'
          }
        ),

        description: new FormControl(model.description),

        price: new FormControl(
          model.price, {
            validators: [Validators.required, Validators.min(0)],
            updateOn: 'blur'
          }
        ),

        idCategory: new FormControl(
          model.idCategory, {
            validators: [Validators.required, Validators.min(1)],
            updateOn: 'blur'
          }
        )
    });
  }

  formToModel = ( form: FormGroup, model: Product ): void => {
    if ( model === null ) {
      model = new Product();
    }

    model.name = form.get('name').value;
    model.description = form.get('description').value;
    model.price = form.get('price').value;
    model.idCategory = form.get('idCategory').value;
  }
}
