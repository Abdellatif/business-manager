import { Injectable } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { Category } from '../../models/catalog/category';
import { BusinessForm } from '../business-form';

@Injectable({
  providedIn: 'root'
})
export class CategoryForm implements BusinessForm {
    createModelForm = (model: Category): FormGroup => {
      const formBuilder = new FormBuilder();
      return formBuilder.group({
          name: new FormControl(
            model.name, {
              validators: [Validators.required],
              updateOn: 'blur'
            }
          ),
          description: new FormControl(model.description)
      });
    }

    formToModel = ( form: FormGroup, model: Category ): void => {
      if ( model === null ) {
          model = new Category();
      }

      model.name = form.get('name').value;
      model.description = form.get('description').value;
    }
}
