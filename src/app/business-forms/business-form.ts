import { FormGroup, FormBuilder } from '@angular/forms';

export interface BusinessForm {
    createModelForm(model: any): FormGroup;
    formToModel(form: FormGroup, model: any);
}
