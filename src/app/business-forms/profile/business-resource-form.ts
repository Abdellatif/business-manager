import { Injectable } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { BusinessResource } from '../../models/profile/business-resource';
import { BusinessForm } from '../business-form';

@Injectable({
  providedIn: 'root'
})
export class BusinessResourceForm implements BusinessForm {
  createModelForm = (model: BusinessResource): FormGroup => {
    const formBuilder = new FormBuilder();
    return formBuilder.group({
        name: new FormControl(
          model.name, {
            validators: [Validators.required],
            updateOn: 'blur'
          }
        )
    });
  }

  formToModel = ( form: FormGroup, model: BusinessResource ) => {
      if ( model === null ) {
        model = new BusinessResource();
      }

      model.name = form.get('name').value;
  }
}
