import { Injectable } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { BusinessAction } from '../../models/profile/business-action';
import { BusinessForm } from '../business-form';

@Injectable({
  providedIn: 'root'
})

export class BusinessActionForm implements BusinessForm {
  createModelForm = (model: BusinessAction): FormGroup => {
    const formBuilder = new FormBuilder();
    return formBuilder.group({
        endpoint: new FormControl(
          model.endpoint, {
              validators: [Validators.required],
              updateOn: 'blur'
          }
        ),

        label: new FormControl(model.label),

        idResource: new FormControl(
          model.idResource, {
              validators: [Validators.required, Validators.min(1)],
              updateOn: 'blur'
          }
        )
    });
  }

  formToModel = ( form: FormGroup, model: BusinessAction ) => {
      if ( model === null ) {
        model = new BusinessAction();
      }

      model.endpoint = form.get('endpoint').value;
      model.label = form.get('label').value;
      model.idResource = form.get('idResource').value;
  }
}
