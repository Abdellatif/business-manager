import { Injectable } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators, FormArray } from '@angular/forms';
import { BusinessAuthority } from '../../models/profile/business-authority';
import { BusinessForm } from '../business-form';

@Injectable({
  providedIn: 'root'
})
export class BusinessAuthorityForm implements BusinessForm {
  createModelForm = (model: BusinessAuthority): FormGroup => {
    const formBuilder = new FormBuilder();
    return formBuilder.group({
      authority: new FormControl(
        model.authority, {
            validators: [Validators.required],
            updateOn: 'blur'
          }
        ),
      actions: formBuilder.array([])
    });
  }

  formToModel = ( form: FormGroup, model: BusinessAuthority ) => {
    if ( model === null ) {
      model = new BusinessAuthority();
    }

    model.authority = form.get('authority').value;
    model.idActions = [];

    const actions: FormArray = form.get('actions') as FormArray;
    actions.controls.forEach((item: FormControl) => {
      model.idActions.push(+item.value);
    });
  }

  onCheckboxChange = ( form: FormGroup, event: any ) => {
    const actions: FormArray = form.get('actions') as FormArray;

    if (event.target.checked) {
      actions.push(new FormControl(event.target.value));
      return;
    }

    let i = 0;
    actions.controls.forEach((item: FormControl) => {
      if (item.value === event.target.value) {
        actions.removeAt(i);
        return;
      }
      i++;
    });
  }
}
