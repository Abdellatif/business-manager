import { Injectable } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { User } from '../../models/users/user';
import { BusinessForm } from '../business-form';

@Injectable({
  providedIn: 'root'
})
export class UserForm implements BusinessForm {
  createModelForm = (model: User): FormGroup => {
    const formBuilder = new FormBuilder();
    return formBuilder.group({
        firstName: new FormControl(
          model.firstName, {
            validators: [Validators.required],
            updateOn: 'blur'
          }
        ),

        lastName: new FormControl(
          model.lastName, {
            validators: [Validators.required],
            updateOn: 'blur'
          }
        ),

        login: new FormControl(
          model.login, {
            validators: [Validators.required],
            updateOn: 'blur'
          }
        ),

        email: new FormControl(
          model.email, {
            validators: [Validators.required],
            updateOn: 'blur'
          }
        ),

        password: new FormControl(model.password),

        reference: new FormControl(
          model.reference, {
            validators: [Validators.required],
            updateOn: 'blur'
          }
        ),

        idAuthority: new FormControl(
          model.idAuthority, {
            validators: [Validators.required, Validators.min(1)],
            updateOn: 'blur'
          }
        )
    });
  }

  formToModel = ( form: FormGroup, model: User ) => {
      if ( model === null ) {
        model = new User();
      }

      model.lastName = form.get('lastName').value;
      model.firstName = form.get('firstName').value;
      model.login = form.get('login').value;
      model.password = form.get('password').value;
      model.email = form.get('email').value;
      model.reference = form.get('reference').value;
      model.idAuthority = form.get('idAuthority').value;
  }
}
