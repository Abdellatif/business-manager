import { Injectable } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { LoginUser } from '../../models/users/login-user';
import { BusinessForm } from '../business-form';

@Injectable({
  providedIn: 'root'
})
export class LoginForm implements BusinessForm {
  createModelForm = (model: LoginUser): FormGroup => {
    const formBuilder = new FormBuilder();
    return formBuilder.group({
        username: new FormControl(
          model.login, {
            validators: [Validators.required],
            updateOn: 'blur'
          }
        ),
        password: new FormControl(
          model.password, {
            validators: [Validators.required],
            updateOn: 'blur'
          }
        )
    });
  }

  formToModel = ( form: FormGroup, model: LoginUser ) => {
    if ( model === null ) {
      model = new LoginUser();
    }

    model.login = form.get('username').value;
    model.password = form.get('password').value;
  }
}
