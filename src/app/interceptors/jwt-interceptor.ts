import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent } from '@angular/common/http';
import { Observable } from 'rxjs';
import { AuthService } from '../services/auth/auth.service';

@Injectable()
export class JwtInterceptor implements HttpInterceptor {
  constructor(private authService: AuthService) {}

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    // Login request
    if ( request.url.endsWith('/oauth/token')  ) {
      request = request.clone({
        setHeaders: {
          'Authorization': 'Basic ' + btoa('business-client:business-secret'),
          'Content-type': 'application/x-www-form-urlencoded'
        }
      });
      return next.handle(request);
    }

    // Other requests
    request = request.clone({
      setHeaders: {
          'Authorization': 'Bearer ' + localStorage.getItem('token')
      }
    });
    return next.handle(request);
  }
}
