import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BusinessAuthoritiesComponent } from './business-authorities.component';

describe('BusinessAuthoritiesComponent', () => {
  let component: BusinessAuthoritiesComponent;
  let fixture: ComponentFixture<BusinessAuthoritiesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BusinessAuthoritiesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BusinessAuthoritiesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
