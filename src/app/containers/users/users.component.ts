import { Component, OnInit } from '@angular/core';
import { select } from '@angular-redux/store';
import { Observable } from 'rxjs';
/************* App state ********/
import { UsersActions } from '../../store/users/users.actions';
import * as usersQueries from '../../store/users/users.queries';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {

  @select(usersQueries.action) appAction: Observable<string>;
  action: string;

  constructor( private usersActions: UsersActions ) {}

  ngOnInit() {
    this.appAction.subscribe((value) => {
      this.action = value;
    });
  }

  // redirect to create new User
  redirectToCreateUser() {
    this.usersActions.redirectToCreateUser();
  }

  // Redirect to Users list
  redirectToUsersList() {
    this.usersActions.redirectToUsersList();
  }
}
