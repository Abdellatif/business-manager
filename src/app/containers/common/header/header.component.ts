import { Component, OnInit } from '@angular/core';

/*************** Actions and queries *******/
import { AuthActions } from '../../../services/auth/auth.actions';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  constructor(private authActions: AuthActions) { }

  ngOnInit() {
    console.log('Header component');
  }

  logout() {
    this.authActions.logout();
  }
}
