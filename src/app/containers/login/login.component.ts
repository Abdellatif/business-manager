import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';

/*************** Models *******/
import { LoginUser } from '../../models/users/login-user';

/*************** Actions and queries *******/
import { AuthActions } from '../../services/auth/auth.actions';

/********* Utils ********/
import { LoginForm } from '../../business-forms/user/login-form';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  loginUser: LoginUser;
  form: FormGroup;

  constructor(
    private authActions: AuthActions,
    private modelForm: LoginForm) {
      this.loginUser = new LoginUser();
  }

  ngOnInit() {
    this.form = this.modelForm.createModelForm(this.loginUser);
  }

  onSubmit() {
    if ( this.form.invalid ) {
      return;
    }

    // Load loginUser from form
    this.modelForm.formToModel(this.form, this.loginUser);
    this.authActions.loginAndLoadUser(this.loginUser);
  }

  onReset() {
    this.form.reset();
  }
}
