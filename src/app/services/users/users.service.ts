/**
 * UsersService
 *
 */
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

/*********** Models **************/
import { User } from '../../models/users/user';

/*********** Configurations ********/
import {APP_URLS} from '../../configuration/urls';

@Injectable()
export class UsersService {
    constructor(private http: HttpClient) {}

    // Get all Users
    public getUsers = (): Observable<User[]> => {
        return this.http
            .get<User[]>(APP_URLS.USERS.GET_ALL);
    }

    // Get User by Id
    public getUserById = (id: number): Observable<User> => {
        return this.http
            .get<User>(APP_URLS.USERS.GET_BY_ID + id);
    }

    // Get User by login
    public getUserByLogin = (login: string): Observable<User> => {
        return this.http
            .get<User>(APP_URLS.USERS.GET_BY_LOGIN + login);
    }

    // Save User
    public saveUser = (user: User): Observable<User> => {
        return this.http
            .post<User>(APP_URLS.USERS.SAVE, user);
    }

    // Remove a User
    public removeUser = (id: number): Observable<User> => {
        return this.http.
            delete<User>(APP_URLS.USERS.REMOVE + id);
    }
}
