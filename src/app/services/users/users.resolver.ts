/**
 * ProductsResolver
 *
 * - ajoute Products
 */
import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable, Subscription } from 'rxjs';
import { UsersActions } from '../../store/users/users.actions';
import { User } from 'src/app/models/users/user';

@Injectable({
    providedIn: 'root'
})

export class UsersResolver implements Resolve<Observable<User[]>| Subscription> {
    constructor(private usersActions: UsersActions) {}
    resolve( route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<User[]> | Subscription {
        return this.usersActions.getUsers();
    }
}
