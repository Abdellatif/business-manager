/**
 * authActions. Not in the store module; login and logout have no state
 */
import { Injectable } from '@angular/core';
import { Router} from '@angular/router';

/******** Models and providers **************/
import { LoginUser } from '../../models/users/login-user';
import { UsersService } from '../users/users.service';
import { AuthService } from './auth.service';

/******** Message handlers **********/
import { handleError } from '../../utils/message-handler';

@Injectable({
    providedIn: 'root'
})

export class AuthActions {
    constructor(
        private authService: AuthService,
        private usersService: UsersService,
        private router: Router
    ) {}

    // Logout
    logout() {
        localStorage.removeItem('token');
        localStorage.removeItem('auth');
        this.router.navigate(['login']);
    }

    // Login
    loginAndLoadUser(loginUser: LoginUser) {
        this.authService.login(loginUser).subscribe(
            (data: any) => {
                // Set token in localeStorage
                if ( data.access_token !== undefined && data.access_token !== null ) {
                    localStorage.setItem('token', data.access_token);
                }

                // Load current user
                this.loadCurrentUser(loginUser.login);
            },
            (error) => {
                handleError(error);
            }
        );
    }

    // Is Authenticated
    isAuthenticated() {
        // isAuthenticated = locastorage must have token and authorities
        const token = localStorage.getItem('token');
        const authoritiy = localStorage.getItem('auth');

        return  token !== undefined && token !== null
            && authoritiy !== undefined && authoritiy !== null;
    }

    // has Authority
    hasAuthority(authority: string) {
        const auth = localStorage.getItem('auth');

        return auth !== undefined && auth !== null && atob(auth) === authority;
    }

    // Load current user
    private loadCurrentUser(login: string) {
        this.usersService.getUserByLogin(login).subscribe(
            (data: any) => {
                localStorage.setItem('auth', btoa(data.authority.authority) );
                this.router.navigate(['']);
            },
            (error) => {
                handleError(error);
            }
        );
    }
}
