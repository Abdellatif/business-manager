/**
 * AuthService
 *
 */
import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';

/*********** Models **************/
import { LoginUser } from '../../models/users/login-user';

/*********** Configurations ********/
import {APP_URLS} from '../../configuration/urls';
import {CREDENTIALS} from '../../configuration/credentials-app';

@Injectable()
export class AuthService {
    constructor(private http: HttpClient) {}

    // Login
    public login = (loginUser: LoginUser): Observable<any> => {
        const body = new HttpParams()
            .set('username', loginUser.login)
            .set('password', loginUser.password)
            .set('grant_type', CREDENTIALS.grant_type);

        return this.http.post(APP_URLS.OAUTH.LOGIN, body.toString());
    }
}
