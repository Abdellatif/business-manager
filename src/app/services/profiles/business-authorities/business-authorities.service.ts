import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

/*********** Models **************/
import { BusinessAuthority } from '../../../models/profile/business-authority';

/*********** Configurations ********/
import {APP_URLS} from '../../../configuration/urls';


@Injectable({
    providedIn: 'root'
})
export class BusinessAuthoritiesService {
    constructor(private http: HttpClient) {}

    // Get all Authories
    public getBusinessAuthorities = (): Observable<BusinessAuthority[]> => {
        return this.http
            .get<BusinessAuthority[]>(APP_URLS.AUTHORITIES.GET_ALL);
    }

    // Get Authority by Id
    public getBusinessAuthorityById = (id: number): Observable<BusinessAuthority> => {
        return this.http
            .get<BusinessAuthority>(APP_URLS.AUTHORITIES.GET_BY_ID +  id);
    }

    // Save Authority
    public saveBusinessAuthority = (businessAuthority: BusinessAuthority): Observable<BusinessAuthority> => {
        return this.http
            .post<BusinessAuthority>(APP_URLS.AUTHORITIES.SAVE, businessAuthority);
    }

    // Remove Authority
    public removeBusinessAuthority = (id: number): Observable<BusinessAuthority> => {
        return this.http.
            delete<BusinessAuthority>(APP_URLS.AUTHORITIES.REMOVE + id);
    }
}
