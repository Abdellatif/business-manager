import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable, Subscription } from 'rxjs';
import { BusinessAuthority } from '../../../models/profile/business-authority';
import { BusinessAuthoritiesActions } from '../../../store/profile/business-authorities/business-authorities.actions';

@Injectable({
    providedIn: 'root'
})

export class BusinessAuthoritiesResolver implements Resolve<Observable<BusinessAuthority[]>| Subscription> {
    constructor(private businessAuthoritiesActions: BusinessAuthoritiesActions) {}
    resolve( route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<BusinessAuthority[]> | Subscription {
        return this.businessAuthoritiesActions.getBusinessAuthorities();
    }
}
