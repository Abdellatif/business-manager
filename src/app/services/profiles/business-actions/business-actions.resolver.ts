import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable, Subscription } from 'rxjs';
import { BusinessAction } from '../../../models/profile/business-action';
import { BusinessActionsActions } from '../../../store/profile/business-actions/business-actions.actions';

@Injectable({
    providedIn: 'root'
})

export class BusinessActionsResolver implements Resolve<Observable<BusinessAction[]>| Subscription> {
    constructor(private businessActionsActions: BusinessActionsActions) {}
    resolve( route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<BusinessAction[]> | Subscription {
        return this.businessActionsActions.getBusinessActions();
    }
}
