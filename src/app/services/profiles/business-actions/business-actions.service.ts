import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

/*********** Models **************/
import { BusinessAction } from '../../../models/profile/business-action';

/*********** Configurations ********/
import {APP_URLS} from '../../../configuration/urls';


@Injectable({
    providedIn: 'root'
})
export class BusinessActionsService {
    constructor(private http: HttpClient) {}

    // Get all actions
    public getBusinessActions = (): Observable<BusinessAction[]> => {
        return this.http
            .get<BusinessAction[]>(APP_URLS.ACTIONS.GET_ALL);
    }

    // Get Action by Id
    public getBusinessActionById = (id: number): Observable<BusinessAction> => {
        return this.http
            .get<BusinessAction>(APP_URLS.ACTIONS.GET_BY_ID +  id);
    }

    // Save Action
    public saveBusinessAction = (businessAction: BusinessAction): Observable<BusinessAction> => {
        return this.http
            .post<BusinessAction>(APP_URLS.ACTIONS.SAVE, businessAction);
    }

    // Remove an Action
    public removeBusinessAction = (id: number): Observable<BusinessAction> => {
        return this.http.
            delete<BusinessAction>(APP_URLS.ACTIONS.REMOVE + id);
    }
}
