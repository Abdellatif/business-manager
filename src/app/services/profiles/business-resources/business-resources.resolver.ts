import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable, Subscription } from 'rxjs';
import { BusinessResource } from '../../../models/profile/business-resource';
import { BusinessResourcesActions } from '../../../store/profile/business-resources/business-resources.actions';

@Injectable({
    providedIn: 'root'
})

export class BusinessResourcesResolver implements Resolve<Observable<BusinessResource[]>| Subscription> {
    constructor(private businessResourcesActions: BusinessResourcesActions) {}
    resolve( route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<BusinessResource[]> | Subscription {
        return this.businessResourcesActions.getBusinessResources();
    }
}
