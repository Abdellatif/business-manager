import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

/*********** Models **************/
import { BusinessResource } from '../../../models/profile/business-resource';

/*********** Configurations ********/
import {APP_URLS} from '../../../configuration/urls';


@Injectable({
    providedIn: 'root'
})
export class BusinessResourcesService {
    constructor(private http: HttpClient) {}

    // Get all Resources
    public getBusinessResources = (): Observable<BusinessResource[]> => {
        return this.http
            .get<BusinessResource[]>(APP_URLS.RESOURCES.GET_ALL);
    }

    // Get Resource by Id
    public getBusinessResourceById = (id: number): Observable<BusinessResource> => {
        return this.http
            .get<BusinessResource>(APP_URLS.RESOURCES.GET_BY_ID +  id);
    }

    // Save Resource
    public saveBusinessResource = (businessResource: BusinessResource): Observable<BusinessResource> => {
        return this.http
            .post<BusinessResource>(APP_URLS.RESOURCES.SAVE, businessResource);
    }

    // Remove a Resource
    public removeBusinessResource = (id: number): Observable<BusinessResource> => {
        return this.http.
            delete<BusinessResource>(APP_URLS.RESOURCES.REMOVE + id);
    }
}
