/**
 * ProductsService
 *
 */
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';

/*********** Models **************/
import { Product } from '../../../models/catalog/product';

/*********** Configurations ********/
import {APP_URLS} from '../../../configuration/urls';

@Injectable()
export class ProductsService {
    constructor(private http: HttpClient) {}

    /*public observableOf = (input: any): Observable<any> => {
        if ( isObservable(input) ) {
            return of([]);
        }
        return result;
    }*/
    // Get all Products
    public getProducts = (): Observable<Product[]> => {
        return this.http
            .get<Product[]>(APP_URLS.PRODUCTS.GET_ALL);
    }

    // Get Product by Id
    public getProductById = (id: number): Observable<Product> => {
        return this.http
            .get<Product>(APP_URLS.PRODUCTS.GET_BY_ID + id);
    }

    // Save Product
    public saveProduct = (product: Product): Observable<Product> => {
        return this.http
            .post<Product>(APP_URLS.PRODUCTS.SAVE, product);
    }

    // Remove a Product
    public removeProduct = (id: number): Observable<Product> => {
        return this.http.
            delete<Product>(APP_URLS.PRODUCTS.REMOVE + id);
    }
}
