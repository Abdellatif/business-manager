/**
 * ProductsResolver
 *
 * - ajoute Products
 */
import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable, Subscription } from 'rxjs';
import { Product } from '../../../models/catalog/product';
import { ProductsActions } from '../../../store/catalog/products/products.actions';

@Injectable({
    providedIn: 'root'
})

export class ProductsResolver implements Resolve<Observable<Product[]>| Subscription> {
    constructor(private productsActions: ProductsActions) {}
    resolve( route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Product[]> | Subscription {
        return this.productsActions.getProducts();
    }
}
