/**
 * CategoriesResolver
 *
 * - ajoute Categories
 */
import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable, Subscription } from 'rxjs';
import { Category } from '../../../models/catalog/category';
import { CategoriesActions } from '../../../store/catalog/categories/categories.actions';

@Injectable({
    providedIn: 'root'
})

export class CategoriesResolver implements Resolve<Observable<Category[]>| Subscription> {
    constructor(private categoriesActions: CategoriesActions) {}
    resolve( route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Category[]> | Subscription {
        return this.categoriesActions.getCategories();
    }
}
