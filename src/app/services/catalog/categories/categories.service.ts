/**
 * CategoriesService
 *
 */
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

/*********** Models **************/
import { Category } from '../../../models/catalog/category';

/*********** Configurations ********/
import {APP_URLS} from '../../../configuration/urls';


@Injectable()
export class CategoriesService {
    constructor(private http: HttpClient) {}

    // Get all categories
    public getCategories = (): Observable<Category[]> => {
        return this.http
            .get<Category[]>(APP_URLS.CATEGORIES.GET_ALL);
    }

    // Get Category by Id
    public getCategoryById = (id: number): Observable<Category> => {
        return this.http
            .get<Category>(APP_URLS.CATEGORIES.GET_BY_ID +  id);
    }

    // Save Category
    public saveCategory = (category: Category): Observable<Category> => {
        return this.http
            .post<Category>(APP_URLS.CATEGORIES.SAVE, category);
    }

    // Remove a category
    public removeCategory = (id: number): Observable<Category> => {
        return this.http.
            delete<Category>(APP_URLS.CATEGORIES.REMOVE + id);
    }
}
