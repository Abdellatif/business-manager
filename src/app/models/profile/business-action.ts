export class BusinessAction {
    id: number;
    endpoint: string;
    label: string;
    idResource: number;
}
