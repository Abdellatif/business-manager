import { BusinessAction } from './business-action';
export class BusinessAuthority {
    id: number;
    authority: string;
    idActions: number[];
}
