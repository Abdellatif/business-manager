import { BusinessAuthority } from '../profile/business-authority';

export class User {
    id: number;
    login: string;
    password: string;
    firstName: string;
    lastName: string;
    email: string;
    reference: string;
    idAuthority: number;
}
