import { Injectable } from '@angular/core';
import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';

import { AuthActions } from '../services/auth/auth.actions';

@Injectable()
export class AdminGuard implements CanActivate {
  constructor(private router: Router, private authActions: AuthActions) {}

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    const admin = 'ADMIN';
    if ( !this.authActions.hasAuthority(admin) ) {
      this.router.navigate(['forbidden']);
      return false;
    }
    return true;
  }
}
