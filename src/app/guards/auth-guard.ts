import { Injectable } from '@angular/core';
import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';

import { AuthActions } from '../services/auth/auth.actions';

@Injectable()
export class AuthGuard implements CanActivate {
  constructor(private router: Router, private authActions: AuthActions) {}

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot, ): boolean {
    if ( !this.authActions.isAuthenticated() ) {
      this.router.navigate(['/login']);
      return false;
    }
    return true;
  }
}
