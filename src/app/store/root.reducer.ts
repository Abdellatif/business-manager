/**
 * rootReducer
 */
import { combineReducers } from 'redux';
import { AppState } from '.';

/********** Reducers *********/
import { categoriesReducer } from './catalog/categories/categories.reducer';
import { productsReducer } from './catalog/products/products.reducer';
import { usersReducer } from './users/users.reducer';
import { businessAuthoritiesReducer } from './profile/business-authorities/business-authorities.reducer';
import { businessResourcesReducer } from './profile/business-resources/business-resources.reducer';
import { businessActionsReducer } from './profile/business-actions/business-actions.reducer';

export const rootReducer = combineReducers<AppState>({
    usersReducer,
    categoriesReducer,
    productsReducer,
    businessAuthoritiesReducer,
    businessResourcesReducer,
    businessActionsReducer
});
