import {AppState} from '../..';

export const getBusinessResources = (state: AppState) => state.businessResourcesReducer.businessResourcesList;
export const getSelectedBusinessResource = (state: AppState) => getBusinessResources(state)[0]; // TODO : to retrieve selected resource
export const action = (state: AppState) => state.businessResourcesReducer.action;
export const businessResource = (state: AppState) => state.businessResourcesReducer.businessResource;
