import { BUSINESS_RESOURCES_ACTIONS } from '../../../configuration/actions';
import { defaultBusinessResourcesState, BusinessResourcesState} from '../..';

export const businessResourcesReducer = ( state: BusinessResourcesState = defaultBusinessResourcesState, action ) => {
    switch (action.type) {
        // Case GET_RESOURCES
        case BUSINESS_RESOURCES_ACTIONS.GET_RESOURCES:
            return {
                ...state,
                businessResourcesList: (action.businessResources === undefined) ? state.businessResourcesList :  action.businessResources,
                action: BUSINESS_RESOURCES_ACTIONS.GET_RESOURCES, // To redirect to resources list
                businessResource: state.businessResource
            };

        // Case SELECT_RESOURCE
        case BUSINESS_RESOURCES_ACTIONS.SELECT_RESOURCE:
            const nextResourcesList = Array.from(state.businessResourcesList);
            return {
                ...state,
                businessResourcesList: state.businessResourcesList,
                action: BUSINESS_RESOURCES_ACTIONS.SELECT_RESOURCE, // To redirect to resource detail
                businessResource: action.businessResource
            };

        // Case CREATE_RESOURCE
        case BUSINESS_RESOURCES_ACTIONS.CREATE_RESOURCE:
            return {
                ...state,
                businessResourcesList: state.businessResourcesList,
                action: BUSINESS_RESOURCES_ACTIONS.CREATE_RESOURCE, // To redirect to create resource
                businessResource: state.businessResource
            };

        // Case SAVE_RESOURCE
        case BUSINESS_RESOURCES_ACTIONS.SAVE_RESOURCE:
            return {
                ...state,
                businessResourcesList: state.businessResourcesList.concat(action.businessResource),
                action: BUSINESS_RESOURCES_ACTIONS.GET_RESOURCES, // To redirect to resources list
                businessResource: state.businessResource
            };

        // Case REMOVE_RESOURCE
        case BUSINESS_RESOURCES_ACTIONS.REMOVE_RESOURCE:
            return {
                ...state,
                businessResourcesList: state.businessResourcesList.filter(item => item.id !== action.id),
                action: BUSINESS_RESOURCES_ACTIONS.GET_RESOURCES, // To redirect to resources list
                businessResource: state.businessResource
            };
        default:
            return state;
    }
};
