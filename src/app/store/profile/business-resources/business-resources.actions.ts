import { Injectable } from '@angular/core';
import { NgRedux } from '@angular-redux/store';

/************** rxjs ************/
import { of } from 'rxjs';

/************* App state ********/
import { AppState } from '../..';

/******** Models and providers **************/
import { BusinessResource } from '../../../models/profile/business-resource';
import { BusinessResourcesService } from '../../../services/profiles/business-resources/business-resources.service';
import { BUSINESS_RESOURCES_ACTIONS } from '../../../configuration/actions';

/******** Message handlers **********/
import { handleError, handleSuccess} from '../../../utils/message-handler';

@Injectable({
    providedIn: 'root'
})

export class BusinessResourcesActions {
    constructor(
        private businessResourcesService: BusinessResourcesService,
        private redux: NgRedux<AppState>
    ) {}

    // Load all resources
    getBusinessResources() {
        // TODO : remove this case throws error in resources.actions
        if (this.redux.getState().businessResourcesReducer.businessResourcesList === undefined) {
            return of([]);
        }

        this.businessResourcesService.getBusinessResources().subscribe(
            (data: BusinessResource[]) => {
                this.redux.dispatch({
                    type: BUSINESS_RESOURCES_ACTIONS.GET_RESOURCES,
                    businessResources: data
                });
            },
            (error) => {
                handleError(error);
            }
        );
    }

    // redirecte to resources list
    redirectToBusinessResourcesList() {
        this.redux.dispatch({
            type: BUSINESS_RESOURCES_ACTIONS.GET_RESOURCES,
        });
    }

    // Select a resource
    selectBusinessResource(businessResource: BusinessResource) {
        return this.businessResourcesService.getBusinessResourceById(businessResource.id).subscribe(
            (data: BusinessResource) => {
                this.redux.dispatch({
                    type: BUSINESS_RESOURCES_ACTIONS.SELECT_RESOURCE,
                    businessResource: data
                });
            },
            (error) => {
                handleError(error);
            }
        );
    }

    // Save a resource
    saveBusinessResource(businessResource: BusinessResource) {
        this.businessResourcesService.saveBusinessResource(businessResource).subscribe(
            // tslint:disable-next-line: no-shadowed-variable
            (businessResource: BusinessResource) => {
                this.redux.dispatch({
                    type: BUSINESS_RESOURCES_ACTIONS.SAVE_RESOURCE,
                    businessResource
                });
                handleSuccess(BUSINESS_RESOURCES_ACTIONS.SAVE_RESOURCE);
            },
            (error) => {
                handleError(error);
            }
        );
    }

    // redirecte to Create new resource
    redirectToCreateBusinessResource() {
        this.redux.dispatch({
            type: BUSINESS_RESOURCES_ACTIONS.CREATE_RESOURCE
        });
    }

    // Remove an resource
    removeBusinessResource(id: number) {
        // tslint:disable-next-line: no-shadowed-variable
        this.businessResourcesService.removeBusinessResource(id).subscribe(
            (data) => {
                this.redux.dispatch({
                    type: BUSINESS_RESOURCES_ACTIONS.REMOVE_RESOURCE,
                    id: data
                });
                handleSuccess(BUSINESS_RESOURCES_ACTIONS.REMOVE_RESOURCE);
            },
            (error) => {
                handleError(error);
            }
        );
    }
}
