import {AppState} from '../..';

export const getBusinessActions = (state: AppState) => state.businessActionsReducer.businessActionsList;
export const getSelectedBusinessAction = (state: AppState) => getBusinessActions(state)[0]; // TODO : to retrieve selected action
export const action = (state: AppState) => state.businessActionsReducer.action;
export const businessAction = (state: AppState) => state.businessActionsReducer.businessAction;
