import { Injectable } from '@angular/core';
import { NgRedux } from '@angular-redux/store';

/************** rxjs ************/
import { of } from 'rxjs';

/************* App state ********/
import { AppState } from '../..';

/******** Models and providers **************/
import { BusinessAction } from '../../../models/profile/business-action';
import { BusinessActionsService } from '../../../services/profiles/business-actions/business-actions.service';
import { BUSINESS_ACTIONS_ACTIONS } from '../../../configuration/actions';

/******** Message handlers **********/
import { handleError, handleSuccess} from '../../../utils/message-handler';

@Injectable({
    providedIn: 'root'
})

export class BusinessActionsActions {
    constructor(
        private businessActionsService: BusinessActionsService,
        private redux: NgRedux<AppState>
    ) {}

    // Load all actions
    getBusinessActions() {
        // TODO : remove this case throws error in actions.actions
        if (this.redux.getState().businessActionsReducer.businessActionsList === undefined) {
            return of([]);
        }

        this.businessActionsService.getBusinessActions().subscribe(
            (data: BusinessAction[]) => {
                this.redux.dispatch({
                    type: BUSINESS_ACTIONS_ACTIONS.GET_ACTIONS,
                    businessActions: data
                });
            },
            (error) => {
                handleError(error);
            }
        );
    }

    // redirecte to actions list
    redirectToBusinessActionsList() {
        this.redux.dispatch({
            type: BUSINESS_ACTIONS_ACTIONS.GET_ACTIONS,
        });
    }

    // Select an action
    selectBusinessAction(businessAction: BusinessAction) {
        return this.businessActionsService.getBusinessActionById(businessAction.id).subscribe(
            (data: BusinessAction) => {
                this.redux.dispatch({
                    type: BUSINESS_ACTIONS_ACTIONS.SELECT_ACTION,
                    businessAction: data
                });
            },
            (error) => {
                handleError(error);
            }
        );
    }

    // Save an action
    saveBusinessAction(businessAction: BusinessAction) {
        this.businessActionsService.saveBusinessAction(businessAction).subscribe(
            // tslint:disable-next-line: no-shadowed-variable
            (data: BusinessAction) => {
                this.redux.dispatch({
                    type: BUSINESS_ACTIONS_ACTIONS.SAVE_ACTION,
                    businessAction: data
                });
                handleSuccess(BUSINESS_ACTIONS_ACTIONS.SAVE_ACTION);
            },
            (error) => {
                handleError(error);
            }
        );
    }

    // redirecte to Create new action
    redirectToCreateBusinessAction() {
        this.redux.dispatch({
            type: BUSINESS_ACTIONS_ACTIONS.CREATE_ACTION
        });
    }

    // Remove an action
    removeBusinessAction(id: number) {
        // tslint:disable-next-line: no-shadowed-variable
        this.businessActionsService.removeBusinessAction(id).subscribe(
            (data) => {
                this.redux.dispatch({
                    type: BUSINESS_ACTIONS_ACTIONS.REMOVE_ACTION,
                    id: data
                });
                handleSuccess(BUSINESS_ACTIONS_ACTIONS.REMOVE_ACTION);
            },
            (error) => {
                handleError(error);
            }
        );
    }
}
