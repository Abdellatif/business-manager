import { BUSINESS_ACTIONS_ACTIONS } from '../../../configuration/actions';
import {defaultBusinessActionsState, BusinessActionsState} from '../..';

export const businessActionsReducer = ( state: BusinessActionsState = defaultBusinessActionsState, action ) => {
    switch (action.type) {
        // Case GET_ACTIONS
        case BUSINESS_ACTIONS_ACTIONS.GET_ACTIONS:
            return {
                ...state,
                businessActionsList: (action.businessActions === undefined) ? state.businessActionsList :  action.businessActions,
                action: BUSINESS_ACTIONS_ACTIONS.GET_ACTIONS, // To redirect to actions list
                businessAction: state.businessAction
            };

        // Case SELECT_ACTION
        case BUSINESS_ACTIONS_ACTIONS.SELECT_ACTION:
            const nextActionsList = Array.from(state.businessActionsList);
            return {
                ...state,
                businessActionsList: state.businessActionsList,
                action: BUSINESS_ACTIONS_ACTIONS.SELECT_ACTION, // To redirect to action detail
                businessAction: action.businessAction
            };

        // Case CREATE_ACTION
        case BUSINESS_ACTIONS_ACTIONS.CREATE_ACTION:
            return {
                ...state,
                businessActionsList: state.businessActionsList,
                action: BUSINESS_ACTIONS_ACTIONS.CREATE_ACTION, // To redirect to create action
                businessAction: state.businessAction
            };

        // Case SAVE_ACTION
        case BUSINESS_ACTIONS_ACTIONS.SAVE_ACTION:
            return {
                ...state,
                businessActionsList: state.businessActionsList.concat(action.businessAction),
                action: BUSINESS_ACTIONS_ACTIONS.GET_ACTIONS, // To redirect to actions list
                businessAction: state.businessAction
            };

        // Case REMOVE_ACTION
        case BUSINESS_ACTIONS_ACTIONS.REMOVE_ACTION:
            return {
                ...state,
                businessActionsList: state.businessActionsList.filter(item => item.id !== action.id),
                action: BUSINESS_ACTIONS_ACTIONS.GET_ACTIONS, // To redirect to actions list
                businessAction: state.businessAction
            };
        default:
            return state;
    }
};
