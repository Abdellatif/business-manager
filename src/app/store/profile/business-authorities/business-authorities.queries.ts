import {AppState} from '../..';

export const getBusinessAuthorities = (state: AppState) => state.businessAuthoritiesReducer.businessAuthoritiesList;
export const getSelectedBusinessAAuthority = 
    (state: AppState) => getBusinessAuthorities(state)[0]; // TODO : to retrieve selected authority
export const action = (state: AppState) => state.businessAuthoritiesReducer.action;
export const businessAuthority = (state: AppState) => state.businessAuthoritiesReducer.businessAuthority;
