import {BUSINESS_AUTHORITIES_ACTIONS} from '../../../configuration/actions';
import {defaultBusinessAuthoritiesState, BusinessAuthoritiesState} from '../..';

export const businessAuthoritiesReducer = ( state: BusinessAuthoritiesState = defaultBusinessAuthoritiesState, action ) => {
    switch (action.type) {
        // Case GET_AUTHORITIES
        case BUSINESS_AUTHORITIES_ACTIONS.GET_AUTHORITIES:
            return {
                ...state,
                businessAuthoritiesList: (action.businessAuthorities === undefined) ?
                    state.businessAuthoritiesList : action.businessAuthorities,
                action: BUSINESS_AUTHORITIES_ACTIONS.GET_AUTHORITIES, // To redirect to authorities list
                businessAuthoity: state.businessAuthority
            };

        // Case SELECT_AUTHORITY
        case BUSINESS_AUTHORITIES_ACTIONS.SELECT_AUTHORITY:
            return {
                ...state,
                businessAuthoritiesList: state.businessAuthoritiesList,
                action: BUSINESS_AUTHORITIES_ACTIONS.SELECT_AUTHORITY, // To redirect to authority detail
                businessAuthority: action.businessAuthority
            };

        // Case CREATE_AUTHORITY
        case BUSINESS_AUTHORITIES_ACTIONS.CREATE_AUTHORITY:
            return {
                ...state,
                businessAuthoritiesList: state.businessAuthoritiesList,
                action: BUSINESS_AUTHORITIES_ACTIONS.CREATE_AUTHORITY, // To redirect to create authority
                businessAuthority: state.businessAuthority
            };

        // Case SAVE_AUTHORITY
        case BUSINESS_AUTHORITIES_ACTIONS.SAVE_AUTHORITY:
            return {
                ...state,
                businessAuthoritiesList: state.businessAuthoritiesList.concat(action.businessAuthoity),
                action: BUSINESS_AUTHORITIES_ACTIONS.GET_AUTHORITIES, // To redirect to authorities list
                businessAuthority: state.businessAuthority
            };

        // Case REMOVE_AUTHORITY
        case BUSINESS_AUTHORITIES_ACTIONS.REMOVE_AUTHORITY:
            return {
                ...state,
                businessAuthoritiesList: state.businessAuthoritiesList.filter(item => item.id !== action.id),
                action: BUSINESS_AUTHORITIES_ACTIONS.GET_AUTHORITIES, // To redirect to authorities list
                businessAuthority: state.businessAuthority
            };
        default:
            return state;
    }
};
