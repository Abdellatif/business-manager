import { Injectable } from '@angular/core';
import { NgRedux } from '@angular-redux/store';

/************** rxjs ************/
import { of } from 'rxjs';

/************* App state ********/
import { AppState } from '../..';

/******** Models and providers **************/
import { BusinessAuthority } from '../../../models/profile/business-authority';
import { BusinessAuthoritiesService } from '../../../services/profiles/business-authorities/business-authorities.service';
import { BUSINESS_AUTHORITIES_ACTIONS } from '../../../configuration/actions';

/******** Message handlers **********/
import { handleError, handleSuccess} from '../../../utils/message-handler';

@Injectable({
    providedIn: 'root'
})

export class BusinessAuthoritiesActions {
    constructor(
        private businessAuthoritiesService: BusinessAuthoritiesService,
        private redux: NgRedux<AppState>
    ) {}

    // Load all authorities
    getBusinessAuthorities() {
        // TODO : remove this case throws error in authorities.actions
        if (this.redux.getState().businessAuthoritiesReducer.businessAuthoritiesList === undefined) {
            return of([]);
        }

        this.businessAuthoritiesService.getBusinessAuthorities().subscribe(
            (data: BusinessAuthority[]) => {
                this.redux.dispatch({
                    type: BUSINESS_AUTHORITIES_ACTIONS.GET_AUTHORITIES,
                    businessAuthorities: data
                });
            },
            (error) => {
                handleError(error);
            }
        );
    }

    // redirecte to authorities list
    redirectToBusinessAuthoritiesList() {
        this.redux.dispatch({
            type: BUSINESS_AUTHORITIES_ACTIONS.GET_AUTHORITIES,
        });
    }

    // Select an authority
    selectBusinessAuthority(businessAuthority: BusinessAuthority) {
        return this.businessAuthoritiesService.getBusinessAuthorityById(businessAuthority.id).subscribe(
            (data: BusinessAuthority) => {
                this.redux.dispatch({
                    type: BUSINESS_AUTHORITIES_ACTIONS.SELECT_AUTHORITY,
                    businessAuthority: data
                });
            },
            (error) => {
                handleError(error);
            }
        );
    }

    // Save an authority
    saveBusinessAAuthority(businessAuthority: BusinessAuthority) {
        this.businessAuthoritiesService.saveBusinessAuthority(businessAuthority).subscribe(
            // tslint:disable-next-line: no-shadowed-variable
            (businessAuthority: BusinessAuthority) => {
                this.redux.dispatch({
                    type: BUSINESS_AUTHORITIES_ACTIONS.SAVE_AUTHORITY,
                    businessAuthority
                });
                handleSuccess(BUSINESS_AUTHORITIES_ACTIONS.SAVE_AUTHORITY);
            },
            (error) => {
                handleError(error);
            }
        );
    }

    // redirecte to Create new authority
    redirectToCreateBusinessAuthority() {
        this.redux.dispatch({
            type: BUSINESS_AUTHORITIES_ACTIONS.CREATE_AUTHORITY
        });
    }

    // Remove an authority
    removeBusinessAuthority(id: number) {
        // tslint:disable-next-line: no-shadowed-variable
        this.businessAuthoritiesService.removeBusinessAuthority(id).subscribe(
            (data) => {
                this.redux.dispatch({
                    type: BUSINESS_AUTHORITIES_ACTIONS.REMOVE_AUTHORITY,
                    id: data
                });
                handleSuccess(BUSINESS_AUTHORITIES_ACTIONS.REMOVE_AUTHORITY);
            },
            (error) => {
                handleError(error);
            }
        );
    }
}
