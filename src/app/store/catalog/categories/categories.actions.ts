/**
 * CategoriesActions
 */
import { Injectable } from '@angular/core';
import { NgRedux } from '@angular-redux/store';

/************** rxjs ************/
import { of } from 'rxjs';

/************* App state ********/
import { AppState } from '../..';

/******** Models and providers **************/
import { Category } from '../../../models/catalog/category';
import { CategoriesService } from '../../../services/catalog/categories/categories.service';
import { CATEGORIES_ACTIONS } from '../../../configuration/actions';

/******** Message handlers **********/
import { handleError, handleSuccess} from '../../../utils/message-handler';

@Injectable({
    providedIn: 'root'
})

export class CategoriesActions {
    constructor(
        private categoriesService: CategoriesService,
        private redux: NgRedux<AppState>
    ) {}

    // Load all categories
    getCategories() {
        // TODO : remove this case throws error in categories.actions
        if (this.redux.getState().categoriesReducer.categoriesList === undefined) {
            return of([]);
        }

        this.categoriesService.getCategories().subscribe(
            (data: Category[]) => {
                this.redux.dispatch({
                    type: CATEGORIES_ACTIONS.GET_CATEGORIES,
                    categories: data
                });
            },
            (error) => {
                handleError(error);
            }
        );
    }

    // redirecte to categories list
    redirectToCategoriesList() {
        this.redux.dispatch({
            type: CATEGORIES_ACTIONS.GET_CATEGORIES,
        });
    }

    // Select a category
    selectCategory(category: Category) {
        return this.categoriesService.getCategoryById(category.id).subscribe(
            (data: Category) => {
                this.redux.dispatch({
                    type: CATEGORIES_ACTIONS.SELECT_CATEGORY,
                    category: data
                });
            },
            (error) => {
                handleError(error);
            }
        );
    }

    // Save a category
    saveCategory(category: Category) {
        this.categoriesService.saveCategory(category).subscribe(
            // tslint:disable-next-line: no-shadowed-variable
            (category: Category) => {
                this.redux.dispatch({
                    type: CATEGORIES_ACTIONS.SAVE_CATEGORY,
                    category
                });
                handleSuccess(CATEGORIES_ACTIONS.SAVE_CATEGORY);
            },
            (error) => {
                handleError(error);
            }
        );
    }

    // redirecte to Create new category
    redirectToCreateCategory() {
        this.redux.dispatch({
            type: CATEGORIES_ACTIONS.CREATE_CATEGORY
        });
    }

    // Remove a category
    removeCategory(id: number) {
        // tslint:disable-next-line: no-shadowed-variable
        this.categoriesService.removeCategory(id).subscribe(
            (data) => {
                this.redux.dispatch({
                    type: CATEGORIES_ACTIONS.REMOVE_CATEGORY,
                    id: data
                });
                handleSuccess(CATEGORIES_ACTIONS.REMOVE_CATEGORY);
            },
            (error) => {
                handleError(error);
            }
        );
    }
}
