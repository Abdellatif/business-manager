import {CATEGORIES_ACTIONS} from '../../../configuration/actions';
import {defaultCategoriesState, CategoriesState} from '../..';

export const categoriesReducer = ( state: CategoriesState = defaultCategoriesState, action ) => {
    switch (action.type) {
        // Case GET_CATEGORIES
        case CATEGORIES_ACTIONS.GET_CATEGORIES:
            return {
                ...state,
                categoriesList: (action.categories === undefined) ? state.categoriesList :  action.categories,
                action: CATEGORIES_ACTIONS.GET_CATEGORIES, // To redirect to categories list
                category: state.category
            };

        // Case SELECT_CATEGORY
        case CATEGORIES_ACTIONS.SELECT_CATEGORY:
            const nextCategoriesList = Array.from(state.categoriesList);
            return {
                ...state,
                categoriesList: state.categoriesList,
                action: CATEGORIES_ACTIONS.SELECT_CATEGORY, // To redirect to categorie detail
                category: action.category
            };

        // Case CREATE_CATEGORY
        case CATEGORIES_ACTIONS.CREATE_CATEGORY:
            return {
                ...state,
                categoriesList: state.categoriesList,
                action: CATEGORIES_ACTIONS.CREATE_CATEGORY, // To redirect to create category
                category: state.category
            };

        // Case SAVE_CATEGORY
        case CATEGORIES_ACTIONS.SAVE_CATEGORY:
            return {
                ...state,
                categoriesList: state.categoriesList.concat(action.category),
                action: CATEGORIES_ACTIONS.GET_CATEGORIES, // To redirect to categories list
                category: state.category
            };

        // Case REMOVE_CATEGORY
        case CATEGORIES_ACTIONS.REMOVE_CATEGORY:
            return {
                ...state,
                categoriesList: state.categoriesList.filter(item => item.id !== action.id),
                action: CATEGORIES_ACTIONS.GET_CATEGORIES, // To redirect to categories list
                category: state.category
            };
        default:
            return state;
    }
};
