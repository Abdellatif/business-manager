/**
 * categoriesQueries
 *
 * - getCategories
 * - getSelectedToys
 * - getToysCount
 * - getTotalPrice
 */
import {AppState} from '../..';

export const getCategories = (state: AppState) => state.categoriesReducer.categoriesList;
export const getSelectedCategories = (state: AppState) => getCategories(state)[0]; // TODO : to retrieve selected category
export const action = (state: AppState) => state.categoriesReducer.action;
export const category = (state: AppState) => state.categoriesReducer.category;
