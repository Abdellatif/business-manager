/**
 * ProductsActions
 */
import { Injectable } from '@angular/core';
import { NgRedux } from '@angular-redux/store';

/************** rxjs ************/
import { of } from 'rxjs';

/************* App state ********/
import { AppState } from '../..';

/******** Models and providers **************/
import { Product } from '../../../models/catalog/product';
import { ProductsService } from '../../../services/catalog/products/products.service';
import { PRODUCTS_ACTIONS } from '../../../configuration/actions';

/******** Message handlers **********/
import { handleError, handleSuccess} from '../../../utils/message-handler';

@Injectable({
    providedIn: 'root'
})

export class ProductsActions {
    constructor(
        private productsService: ProductsService,
        private redux: NgRedux<AppState>
    ) {}

    // Load all Products
    getProducts() {
        // TODO : remove this case throws error in products.actions
        if (this.redux.getState().productsReducer.productsList === undefined) {
            return of([]);
        }
        this.productsService.getProducts().subscribe(
            (data: Product[]) => {
                this.redux.dispatch({
                    type: PRODUCTS_ACTIONS.GET_PRODUCTS,
                    products: data
                });
            },
            (error) => {
                handleError(error);
            }
        );
    }

    // redirecte to Products list
    redirectToProductsList() {
        this.redux.dispatch({
            type: PRODUCTS_ACTIONS.GET_PRODUCTS,
        });
    }

    // Select a Product
    selectProduct(product: Product) {
        return this.productsService.getProductById(product.id).subscribe(
            (data: Product) => {
                this.redux.dispatch({
                    type: PRODUCTS_ACTIONS.SELECT_PRODUCT,
                    product: data
                });
            },
            (error) => {
                handleError(error);
            }
        );
    }

    // Save a Product
    saveProduct(product: Product) {
        this.productsService.saveProduct(product).subscribe(
            (data: Product) => {
                this.redux.dispatch({
                    type: PRODUCTS_ACTIONS.SAVE_PRODUCT,
                    product: data
                });
                handleSuccess(PRODUCTS_ACTIONS.SAVE_PRODUCT);
            },
            (error) => {
                handleError(error);
            }
        );
    }

    // redirecte to Create new Product
    redirectToCreateProduct() {
        this.redux.dispatch({
            type: PRODUCTS_ACTIONS.CREATE_PRODUCT
        });
    }

    // Remove a Product
    removeProduct(id: number) {
        this.productsService.removeProduct(id).subscribe(
            (data) => {
                this.redux.dispatch({
                    type: PRODUCTS_ACTIONS.REMOVE_PRODUCT,
                    id
                });
            },
            (error) => {
                handleError(error);
            }
        );
    }
}
