/**
 * ProductsQueries
 */
import {AppState} from '../..';

export const getProducts = (state: AppState) => state.productsReducer.productsList;
export const getSelectedProducts = (state: AppState) => getProducts(state)[0]; // TODO : to retrieve selected product
export const action = (state: AppState) => state.productsReducer.action;
export const product = (state: AppState) => state.productsReducer.product;
