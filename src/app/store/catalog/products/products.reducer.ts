import {PRODUCTS_ACTIONS} from '../../../configuration/actions';
import {defaultProductsState, ProductsState} from '../..';

export const productsReducer = (state: ProductsState = defaultProductsState, action) => {
    switch (action.type) {
        // Case GET_productS
        case PRODUCTS_ACTIONS.GET_PRODUCTS:
            return {
                ...state,
                productsList: (action.products === undefined) ? state.productsList :  action.products,
                action: PRODUCTS_ACTIONS.GET_PRODUCTS, // To redirect to products list
                product: state.product
            };

        // Case SELECT_product
        case PRODUCTS_ACTIONS.SELECT_PRODUCT:
            return {
                ...state,
                productsList: state.productsList,
                action: PRODUCTS_ACTIONS.SELECT_PRODUCT, // To redirect to product detail
                product: action.product
            };

        // Case CREATE_product
        case PRODUCTS_ACTIONS.CREATE_PRODUCT:
            return {
                ...state,
                productsList: state.productsList,
                action: PRODUCTS_ACTIONS.CREATE_PRODUCT, // To redirect to create product
                product: state.product
            };

        // Case SAVE_product
        case PRODUCTS_ACTIONS.SAVE_PRODUCT:
            return {
                ...state,
                productsList: state.productsList.concat(action.product),
                action: PRODUCTS_ACTIONS.GET_PRODUCTS, // To redirect to products list
                product: state.product
            };

        // Case REMOVE_product
        case PRODUCTS_ACTIONS.REMOVE_PRODUCT:
            return {
                ...state,
                productsList: state.productsList.filter(item => item.id !== action.id),
                action: PRODUCTS_ACTIONS.GET_PRODUCTS, // To redirect to products list
                product: state.product
            };

        default:
            return state;
    }
};
