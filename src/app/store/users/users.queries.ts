/**
 * UsersQueries
 */
import {AppState} from '..';

export const getUsers = (state: AppState) => state.usersReducer.usersList;
export const getSelectedUser = (state: AppState) => getUsers(state)[0]; // TODO : to retrieve selected user
export const action = (state: AppState) => state.usersReducer.action;
export const user = (state: AppState) => state.usersReducer.user;
