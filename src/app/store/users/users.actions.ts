/**
 * userActions
 */
import { Injectable } from '@angular/core';
import { NgRedux } from '@angular-redux/store';

/************** rxjs ************/
import { of } from 'rxjs';

/************* App state ********/
import { AppState } from '..';

/******** Models and providers **************/
import { User } from '../../models/users/user';
import { UsersService } from '../../services/users/users.service';
import { USERS_ACTIONS } from '../../configuration/actions';

/******** Message handlers **********/
import { handleError, handleSuccess} from '../../utils/message-handler';

@Injectable({
    providedIn: 'root'
})

export class UsersActions {
    constructor(
        private usersService: UsersService,
        private redux: NgRedux<AppState>
    ) {}

    // Load all users
    getUsers() {
        // TODO : remove this case throws error in users.actions
        if (this.redux.getState().usersReducer.usersList === undefined) {
            return of([]);
        }
        this.usersService.getUsers().subscribe(
            (data: User[]) => {
                this.redux.dispatch({
                    type: USERS_ACTIONS.GET_USERS,
                    users: data
                });
            },
            (error) => {
                handleError(error);
            }
        );
    }

    // redirecte to Users list
    redirectToUsersList() {
        this.redux.dispatch({
            type: USERS_ACTIONS.GET_USERS,
        });
    }

    // Select a User
    selectUser(user: User) {
        return this.usersService.getUserById(user.id).subscribe(
            (data: User) => {
                this.redux.dispatch({
                    type: USERS_ACTIONS.SELECT_USER,
                    user: data
                });
            },
            (error) => {
                handleError(error);
            }
        );
    }

    // Save a User
    saveUser(user: User) {
        this.usersService.saveUser(user).subscribe(
            (data: User) => {
                this.redux.dispatch({
                    type: USERS_ACTIONS.SAVE_USER,
                    user: data
                });
                handleSuccess(USERS_ACTIONS.SAVE_USER);
            },
            (error) => {
                handleError(error);
            }
        );
    }

    // redirecte to Create new User
    redirectToCreateUser() {
        this.redux.dispatch({
            type: USERS_ACTIONS.CREATE_USER
        });
    }

    // Remove a User
    removeUser(id: number) {
        this.usersService.removeUser(id).subscribe(
            (data) => {
                this.redux.dispatch({
                    type: USERS_ACTIONS.REMOVE_USER,
                    id
                });
            },
            (error) => {
                handleError(error);
            }
        );
    }
}
