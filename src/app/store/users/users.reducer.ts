import {USERS_ACTIONS} from '../../configuration/actions';
import {defaultUsersState, UsersState} from '..';

export const usersReducer = (state: UsersState = defaultUsersState, action) => {
    switch (action.type) {
        // Case GET_USERS
        case USERS_ACTIONS.GET_USERS:
            return {
                ...state,
                usersList: (action.users === undefined) ? state.usersList :  action.users,
                action: USERS_ACTIONS.GET_USERS, // To redirect to users list
                user: state.user
            };

        // Case SELECT_USER
        case USERS_ACTIONS.SELECT_USER:
            return {
                ...state,
                usersList: state.usersList,
                action: USERS_ACTIONS.SELECT_USER, // To redirect to user detail
                user: action.user
            };

        // Case CREATE_USER
        case USERS_ACTIONS.CREATE_USER:
            return {
                ...state,
                usersList: state.usersList,
                action: USERS_ACTIONS.CREATE_USER, // To redirect to create user
                user: state.user
            };

        // Case SAVE_USER
        case USERS_ACTIONS.SAVE_USER:
            return {
                ...state,
                usersList: state.usersList.concat(action.user),
                action: USERS_ACTIONS.GET_USERS, // To redirect to users list
                user: state.user
            };

        // Case REMOVE_USER
        case USERS_ACTIONS.REMOVE_USER:
            return {
                ...state,
                usersList: state.usersList.filter(item => item.id !== action.id),
                action: USERS_ACTIONS.GET_USERS, // To redirect to users list
                user: state.user
            };

        default:
            return state;
    }
};
