import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import {NgReduxModule, NgRedux, DevToolsExtension} from '@angular-redux/store';

import {rootReducer} from './root.reducer';
import { AppState, defaultAppState } from '.' ;

import { environment } from 'src/environments/environment';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    NgReduxModule
  ]
})
export class StoreModule {
  constructor(private redux: NgRedux<AppState>, devTools: DevToolsExtension) {
    const enhancers = [];
    if (!environment.production) {
        enhancers.push(devTools.isEnabled() ? devTools.enhancer() : (f) => f);
    }

    redux.configureStore(
        rootReducer,
        defaultAppState,
        [],
        enhancers
    );
  }
}
