/**
 * - define interfaces States and defaultStates
 * - definir interface AppState and defaultAppState
 *
 */

import { Product } from '../models/catalog/product';
import { Category } from '../models/catalog/category';
import { User } from '../models/users/user';
import { BusinessAuthority } from '../models/profile/business-authority';
import { BusinessResource } from '../models/profile/business-resource';
import { BusinessAction } from '../models/profile/business-action';

/*************Users States *********/
export interface UsersState {
    usersList: User[];
    action: string;
    user: User;
}

export const defaultUsersState: UsersState = {
    usersList: [],
    action: '',
    user : new User()
};

/************* Catalg *********/
/*------- Products States ---*/
export interface ProductsState {
    productsList: Product[];
    action: string;
    product: Product;
}
export const defaultProductsState: ProductsState = {
    productsList: [],
    action: '',
    product : new Product()
};

/*-------  Categories States ---*/
export interface CategoriesState {
    categoriesList: Category[];
    action: string;
    category: Category;
}
export const defaultCategoriesState: CategoriesState = {
    categoriesList: [],
    action: '',
    category: new Category()
};

/*************Profile *********/
/*------- BusinessAuthorities States ---*/
export interface BusinessAuthoritiesState {
    businessAuthoritiesList: BusinessAuthority[];
    action: string;
    businessAuthority: BusinessAuthority;
}
export const defaultBusinessAuthoritiesState: BusinessAuthoritiesState = {
    businessAuthoritiesList: [],
    action: '',
    businessAuthority : new BusinessAuthority()
};

/*------- BusinessResources States ---*/
export interface BusinessResourcesState {
    businessResourcesList: BusinessResource[];
    action: string;
    businessResource: BusinessResource;
}
export const defaultBusinessResourcesState: BusinessResourcesState = {
    businessResourcesList: [],
    action: '',
    businessResource : new BusinessResource()
};

/*------- BusinessActions States ---*/
export interface BusinessActionsState {
    businessActionsList: BusinessAction[];
    action: string;
    businessAction: BusinessAction;
}
export const defaultBusinessActionsState: BusinessActionsState = {
    businessActionsList: [],
    action: '',
    businessAction : new BusinessAction()
};

/************* App States *********/
export interface AppState {
    usersReducer: UsersState;
    productsReducer: ProductsState;
    categoriesReducer: CategoriesState;
    businessAuthoritiesReducer: BusinessAuthoritiesState;
    businessResourcesReducer: BusinessResourcesState;
    businessActionsReducer: BusinessActionsState;
}

export const defaultAppState: AppState = {
    usersReducer: defaultUsersState,
    productsReducer: defaultProductsState,
    categoriesReducer: defaultCategoriesState,
    businessAuthoritiesReducer: defaultBusinessAuthoritiesState,
    businessResourcesReducer: defaultBusinessResourcesState,
    businessActionsReducer: defaultBusinessActionsState
};
