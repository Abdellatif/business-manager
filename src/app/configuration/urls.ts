export const APP_URLS = {
    /** ---------- Catalog -------- */
    CATEGORIES : {
        GET_ALL: 'http://localhost:9990/catalog/categories/',
        REMOVE: 'http://localhost:9990/catalog/categories/delete/',
        SAVE: 'http://localhost:9990/catalog/categories/create',
        GET_BY_ID: 'http://localhost:9990/catalog/categories/'
    },

    PRODUCTS: {
        GET_ALL: 'http://localhost:9990/catalog/products/',
        REMOVE: 'http://localhost:9990/catalog/products/delete/',
        SAVE: 'http://localhost:9990/catalog/products/create',
        GET_BY_ID: 'http://localhost:9990/catalog/products/'
    },

    /** ---------- Auth -------- */
    OAUTH: {
        LOGIN: 'http://localhost:9990/oauth/oauth/token'
    },

    /** ---------- Profile -------- */
    AUTHORITIES : {
        GET_ALL: 'http://localhost:9990/profile/authorities/',
        REMOVE: 'http://localhost:9990/profile/authorities/delete/',
        SAVE: 'http://localhost:9990/profile/authorities/create',
        GET_BY_ID: 'http://localhost:9990/profile/authorities/'
    },

    RESOURCES : {
        GET_ALL: 'http://localhost:9990/profile/resources/',
        REMOVE: 'http://localhost:9990/profile/resources/delete/',
        SAVE: 'http://localhost:9990/profile/resources/create',
        GET_BY_ID: 'http://localhost:9990/profile/resources/'
    },

    ACTIONS : {
        GET_ALL: 'http://localhost:9990/profile/actions/',
        REMOVE: 'http://localhost:9990/profile/actions/delete/',
        SAVE: 'http://localhost:9990/profile/actions/create',
        GET_BY_ID: 'http://localhost:9990/profile/actions/'
    },

    /** ---------- Users -------- */
    USERS: {
        GET_ALL: 'http://localhost:9990/user/members/',
        REMOVE: 'http://localhost:9990/user/members/delete/',
        SAVE: 'http://localhost:9990/user/members/create',
        GET_BY_ID: 'http://localhost:9990/user/members/',
        GET_BY_LOGIN: 'http://localhost:9990/user/members/username/'
    },
};
