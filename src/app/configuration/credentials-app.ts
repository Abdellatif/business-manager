export const CREDENTIALS = {
    client_id: 'business_client',
    client_secret: 'business_secret',
    grant_type: 'password',
    scope: 'trust'
};
